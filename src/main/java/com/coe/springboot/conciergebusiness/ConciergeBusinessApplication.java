package com.coe.springboot.conciergebusiness;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConciergeBusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConciergeBusinessApplication.class, args);
	}

}
